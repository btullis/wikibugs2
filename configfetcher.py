#!/usr/bin/env python

import json
import os

WIKIBUGS_PREFIX = "WIKIBUGS_"


class ConfigFetcher(object):
    """
    A simple wrapper around a JSON file,
    in the future it might do more fancy things
    maybe!
    """

    def __init__(self, filename=None):
        if f"{WIKIBUGS_PREFIX}ENV_CONFIG" in os.environ:
            self.file_options = {
                k.split(WIKIBUGS_PREFIX, 1)[1]: v
                for (k, v) in os.environ.items()
                if WIKIBUGS_PREFIX in k
            }
            return

        if not filename:
            filename = os.path.join(os.path.dirname(__file__), "config.json")

        with open(filename) as f:
            self.file_options = json.load(f)

    def get(self, name):
        return self.file_options.get(name)
